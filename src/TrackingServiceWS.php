<?php

declare(strict_types=1);

namespace Drupal\commerce_chronopost;

use WsdlToPhp\PackageBase\AbstractSoapClientBase;
use Maetva\Chronopost\TrackingServiceWS\ClassMap;
use Maetva\Chronopost\TrackingServiceWS\ServiceType\Service;

/**
 * The tracking webservice consumer.
 */
class TrackingServiceWS extends Service {

  /**
   * {@inheritDoc}
   */
  public function initSoapClient(array $options = NULL): void {
    $options = [
      AbstractSoapClientBase::WSDL_CLASSMAP => ClassMap::get(),
      AbstractSoapClientBase::WSDL_URL => dirname(__DIR__).DIRECTORY_SEPARATOR.'wsdl'.DIRECTORY_SEPARATOR.'TrackingServiceWS.wsdl',
    ];

    parent::initSoapClient($options);
  }

}

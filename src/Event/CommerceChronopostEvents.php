<?php

declare(strict_types = 1);

namespace Drupal\commerce_chronopost\Event;

/**
 * Defines events for the Chronopost module.
 */
final class CommerceChronopostEvents {

  /**
   * Event allowing alterations of the shipment request parameters.
   *
   * @Event
   *
   * @see \Drupal\commerce_chronopost\Event\PreShipmentRequestEvent
   */
  const PRE_SHIPMENT_REQUEST = 'commerce_chronopost.pre_shipment_request';

}

<?php

declare(strict_types = 1);

namespace Drupal\commerce_chronopost\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\Chronopost\ShippingServiceWS\StructType\ShippingMultiParcelV4;

/**
 * Defines pre shipment request event.
 *
 * @see \Drupal\commerce_chronopost\Event\CommerceDhlExpressEvents
 * @see \Drupal\commerce_chronopost\ExpressRateBook::getProcessShipmentRequestParameters()
 */
class PreShipmentRequestEvent extends Event {

  /**
   * The shipment entity.
   *
   * @var \Drupal\commerce_shipping\Entity\ShipmentInterface
   */
  protected $shipment;

  /**
   * The shipment request parameters.
   *
   * @var \Maetva\Chronopost\ShippingServiceWS\StructType\ShippingMultiParcelV4
   */
  protected $parameters;

  /**
   * Constructs a new PreShipmentRequestEvent instance.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   * @param \Maetva\Chronopost\ShippingServiceWS\StructType\ShippingMultiParcelV4 $parameters
   *   The shipment request parameters.
   */
  public function __construct(ShipmentInterface $shipment, ShippingMultiParcelV4 $parameters) {
    $this->shipment = $shipment;
    $this->parameters = $parameters;
  }

  /**
   * Gets the shipment request parameters.
   *
   * @return \Maetva\Chronopost\ShippingServiceWS\StructType\ShippingMultiParcelV4
   *   The shipment request parameters.
   */
  public function getParameters(): ShippingMultiParcelV4 {
    return $this->parameters;
  }

  /**
   * Sets shipment request parameters.
   *
   * @param \Maetva\Chronopost\ShippingServiceWS\StructType\ShippingMultiParcelV4 $parameters
   *   The shipment request parameters.
   *
   * @return $this
   */
  public function setParameters(ShippingMultiParcelV4 $parameters): self {
    $this->parameters = $parameters;

    return $this;
  }

  /**
   * Gets the shipment entity.
   *
   * @return \Drupal\commerce_shipping\Entity\ShipmentInterface
   *   The shipment entity.
   */
  public function getShipment(): ShipmentInterface {
    return $this->shipment;
  }

}

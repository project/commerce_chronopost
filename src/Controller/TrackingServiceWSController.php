<?php

declare(strict_types = 1);

namespace Drupal\commerce_chronopost\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\commerce_chronopost\TrackingServiceWS;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Manages Chronopost shipping WS related operations.
 */
class TrackingServiceWSController extends ControllerBase {

  const LOGGER_CHANNEL = 'commerce_chronopost';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The Chronopost shipping WS.
   *
   * @var \Drupal\commerce_chronopost\TrackingServiceWS
   */
  protected $trackingServiceWS;

  /**
   * The module related logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannel;

  /**
   * Create a new TrackingServiceWSController instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\commerce_chronopost\TrackingServiceWS $tracking_service_ws
   *   The Chronopost tracking manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger channel factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger, TrackingServiceWS $tracking_service_ws, LoggerChannelFactoryInterface $logger_channel_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->trackingServiceWS = $tracking_service_ws;
    $this->loggerChannel = $logger_channel_factory->get(self::LOGGER_CHANNEL);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('messenger'),
      $container->get('commerce_chronopost.tracking_service_ws'),
      $container->get('logger.factory')
    );
  }

  /**
   * Cancels a Chronopost shipping.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $commerce_shipment
   *   The related commerce shipment entity.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response to the shipment canonical route.
   */
  public function cancelShipment(ShipmentInterface $commerce_shipment): RedirectResponse {
    // Cancel a potential existing Chronopost shipment.
    if (!$tracking_code = $commerce_shipment->getTrackingCode()) {
      $this->messenger->addError(
        $this->t('The Chronopost shipment cannnot be cancel. There is no tracking code.')
      );

      return $this->redirect('entity.commerce_shipment.canonical', [
        'commerce_order' => $commerce_shipment->getOrder()->id(),
        'commerce_shipment' => $commerce_shipment->id(),
      ]);
    }

    $cancel_response = $commerce_shipment
      ->getShippingMethod()
      ->getPlugin()
      ->cancelRemote($commerce_shipment)
      ->getReturn();

    // Return early if the existing Chronopost shipment cannot be canceled.
    if (!empty($cancel_response->getErrorCode())) {
      // $this->loggerChannel->notice($cancel_response->getErrorMessage());
      $this->messenger->addError($this->t('Chronopost shipping cancelation failed.'));
      return $this->redirect('entity.commerce_shipment.canonical', [
        'commerce_order' => $commerce_shipment->getOrder()->id(),
        'commerce_shipment' => $commerce_shipment->id(),
      ]);
    }

    // $commerce_shipment->set('state', 'draft'); // @todo Work with state ?
    // Log the cancelation into the parent order.
    $this->entityTypeManager
      ->getStorage('commerce_log')
      ->generate($commerce_shipment->getOrder(), 'commerce_chronopost_cancel_skybill', [
        'tracking_code' => $tracking_code,
      ])
      ->save();

    return $this->redirect('entity.commerce_shipment.canonical', [
      'commerce_order' => $commerce_shipment->getOrder()->id(),
      'commerce_shipment' => $commerce_shipment->id(),
    ]);
  }

  /**
   * Tracks a Chronopost shipping.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $commerce_shipment
   *   The related commerce shipment entity.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response to the shipment canonical route.
   */
  public function trackShipment(ShipmentInterface $commerce_shipment): RedirectResponse {
    $commerce_shipment->getShippingMethod()->getPlugin()->track($commerce_shipment);

    return $this->redirect(
      'entity.commerce_shipment.canonical',
      [
        'commerce_order' => $commerce_shipment->getOrder()->id(),
        'commerce_shipment' => $commerce_shipment->id(),
      ]
    );
  }

}

<?php

declare(strict_types = 1);

namespace Drupal\commerce_chronopost\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_chronopost\Form\ShippingMultiParcelV4Form;

/**
 * Manages Chronopost shipping WS related operations.
 */
class ShippingServiceWSController extends ControllerBase {

  /**
   * The module related logger channel.
   */
  const LOGGER_CHANNEL = 'commerce_chronopost';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The module related logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannel;

  /**
   * Create a new ShippingManagerController instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger channel factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger, LoggerChannelFactoryInterface $logger_channel_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->loggerChannel = $logger_channel_factory->get(self::LOGGER_CHANNEL);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('messenger'),
      $container->get('logger.factory')
    );
  }

  /**
   * Requests a Chronopost shipping.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $commerce_shipment
   *   The related commerce shipment entity.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response to the shipment canonical route.
   */
  public function createShipment(ShipmentInterface $commerce_shipment) {
    // Display a form for required additionnal data.
    return \Drupal::formBuilder()->getForm(ShippingMultiParcelV4Form::class, $commerce_shipment);
  }

  /**
   * Gets a Chronopost shipment PDF label.
   *
   * @param Drupal\commerce_shipping\Entity\ShipmentInterface $commerce_shipment
   *   The related commerce shipment entity.
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
   *   The Chronopost label file.
   */
  public function downloadLabel(ShipmentInterface $commerce_shipment): BinaryFileResponse {
    $pdf_label_dir = 'private://chronopost';
    $pdf_label_filename = implode('-', [
      $commerce_shipment->getOrder()->id(),
      $commerce_shipment->id(),
      $commerce_shipment->getTrackingCode(),
    ]) . '.pdf';
    $headers = [
      'Content-Type' => 'application/pdf',
      'Content-Disposition' => 'attachment; filename="' . $pdf_label_filename . '"',
    ];

    return new BinaryFileResponse($pdf_label_dir . '/' . $pdf_label_filename, 200, $headers, TRUE);
  }

}

<?php

declare(strict_types=1);

namespace Drupal\commerce_chronopost;

use WsdlToPhp\PackageBase\AbstractSoapClientBase;
use Maetva\Chronopost\ShippingServiceWS\ClassMap;
use Maetva\Chronopost\ShippingServiceWS\ServiceType\Service;

/**
 * The shipping webservice consumer.
 */
class ShippingServiceWS extends Service {

  /**
   * {@inheritDoc}
   */
  public function initSoapClient(array $options = NULL): void {
    $options = [
      AbstractSoapClientBase::WSDL_CLASSMAP => ClassMap::get(),
      AbstractSoapClientBase::WSDL_URL => dirname(__DIR__).DIRECTORY_SEPARATOR.'wsdl'.DIRECTORY_SEPARATOR.'ShippingServiceWS.wsdl',
    ];

    parent::initSoapClient($options);
  }

}

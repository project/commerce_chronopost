<?php

declare(strict_types=1);

namespace Drupal\commerce_chronopost\Factory\ShippingServiceWS;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\Chronopost\ShippingServiceWS\StructType\ShipperValueV2;

/**
 * ShipperValueV2 factory.
 */
class ShipperValueV2Factory extends ShipperValueV2 {

  /**
   * Constructs a new ShipperValueV2 instance from a shipment entity.
   *
   * @param ShipmentInterface $shipment
   *
   * @return ShipperValueV2
   */
  public static function createFromShipment(ShipmentInterface $shipment): ShipperValueV2 {
    $shipping_method_config = $shipment->getShippingMethod()->getPlugin()->getConfiguration();
    $shipper_config = $shipping_method_config['api_information']['shipper'];
    $country_list = \Drupal::getContainer()->get('country_manager')->getList();

    $shipper_value = (new ShipperValueV2)
      ->setShipperCivility($shipper_config['civility'])
      ->setShipperAdress1($shipper_config['address']['address_line1'])
      ->setShipperAdress2($shipper_config['address']['address_line2'])
      ->setShipperCity($shipper_config['address']['locality'])
      ->setShipperZipCode($shipper_config['address']['postal_code'])
      ->setShipperCountry($shipper_config['address']['country_code'])
      ->setShipperCountryName(
        $country_list[$shipper_config['address']['country_code']]->render()
      )
      ->setShipperEmail($shipper_config['email'])
      ->setShipperPhone($shipper_config['phone'])
      ->setShipperName($shipper_config['address']['organization'])
      ->setShipperContactName($shipper_config['address']['given_name'] . ' ' . $shipper_config['address']['family_name'])
      ->setShipperPreAlert((int) $shipper_config['pre_alert']);

    return $shipper_value;
  }

}

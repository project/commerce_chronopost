<?php

declare(strict_types=1);

namespace Drupal\commerce_chronopost\Factory\ShippingServiceWS;

use Drupal\physical\LengthUnit;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\Chronopost\ShippingServiceWS\StructType\EsdValue3;

/**
 * EsdValue3 Factory.
 */
class EsdValue3Factory extends EsdValue3 {

  /**
   * Constructs a new EsdValue3 instance from a shipment entity.
   *
   * @param ShipmentInterface $shipment
   *
   * @return EsdValue3
   */
  public static function createFromShipment(ShipmentInterface $shipment): EsdValue3 {
    $package = $shipment->getPackageType();
    $esd_value = (new EsdValue3)
      ->setHeight((float) $package->getHeight()->convert(LengthUnit::CENTIMETER)->getNumber())
      ->setWidth((float) $package->getWidth()->convert(LengthUnit::CENTIMETER)->getNumber())
      ->setLength((float) $package->getLength()->convert(LengthUnit::CENTIMETER)->getNumber())
      ->setNombreDePassageMaximum(1)
      ->setLtAImprimerParChronopost(FALSE);

    return $esd_value;
  }

}

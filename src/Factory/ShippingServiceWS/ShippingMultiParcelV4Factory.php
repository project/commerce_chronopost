<?php

declare(strict_types=1);

namespace Drupal\commerce_chronopost\Factory\ShippingServiceWS;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\Chronopost\ShippingServiceWS\StructType\ShippingMultiParcelV4;

/**
 * ShippingMultiParcelV4 factory.
 */
class ShippingMultiParcelV4Factory extends ShippingMultiParcelV4 {

  /**
   * Constructs a new ShippingMultiParcelV4 instance from a shipment entity.
   *
   * @param ShipmentInterface $shipment
   *
   * @return ShippingMultiParcelV4
   */
  public static function createFromShipment(ShipmentInterface $shipment): ShippingMultiParcelV4 {
    $shipping_method_config = $shipment->getShippingMethod()->getPlugin()->getConfiguration();
    $mode = $shipping_method_config['mode'];
    $shipping_multi_parcel = (new ShippingMultiParcelV4)
      // ->setEsdValue(EsdValue3Factory::createFromShipment($shipment)) // @todo Conditionnate the EsdValue on the collect type.
      ->setHeaderValue(HeaderValueFactory::createFromShipment($shipment))
      ->setShipperValue([ShipperValueV2Factory::createFromShipment($shipment)])
      ->setCustomerValue(CustomerValueFactory::createFromShipment($shipment))
      ->setRecipientValue([RecipientValueV2Factory::createFromShipment($shipment)])
      ->setRefValue([RefValueV2Factory::createFromShipment($shipment)])
      ->setSkybillValue([SkybillWithDimensionsValueV6Factory::createFromShipment($shipment)])
      ->setSkybillParamsValue(SkybillParamsValueV2Factory::createFromShipment($shipment))
      ->setPassword($shipping_method_config['api_information']['credentials'][$mode]['password'])
      ->setScheduledValue([ScheduledValueFactory::createFromShipment($shipment)])
      ->setNumberOfParcel(1)
      ->setModeRetour('2');

    return $shipping_multi_parcel;
  }

}

<?php

declare(strict_types=1);

namespace Drupal\commerce_chronopost\Factory\ShippingServiceWS;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\Chronopost\ShippingServiceWS\StructType\RefValueV2;

/**
 * RefValueV2 factory.
 */
class RefValueV2Factory extends RefValueV2 {

  /**
   * Constructs a new RefValueV2 instance from a shipment entity.
   *
   * @param ShipmentInterface $shipment
   *
   * @return RefValueV2
   */
  public static function createFromShipment(ShipmentInterface $shipment): RefValueV2 {
    $order = $shipment->getOrder();
    $ref_value = (new RefValueV2)
      ->setCustomerSkybillNumber($order->id() . '-' . $shipment->id())
      ->setShipperRef($shipment->getOrder()->getStore()->id())
      ->setRecipientRef($order->getCustomer()->id() . '-' . $shipment->getShippingProfile()->id());

    return $ref_value;
  }

}

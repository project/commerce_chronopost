<?php

declare(strict_types=1);

namespace Drupal\commerce_chronopost\Factory\ShippingServiceWS;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\Chronopost\ShippingServiceWS\StructType\CustomerValue;

/**
 * CustomerValue Factory.
 */
class CustomerValueFactory extends CustomerValue {

  /**
   * Constructs a new CustomerValue instance from a shipment entity.
   *
   * @param ShipmentInterface $shipment
   *
   * @return CustomerValue
   */
  public static function createFromShipment(ShipmentInterface $shipment): CustomerValue {
    $country_list = \Drupal::getContainer()->get('country_manager')->getList();
    $shipping_method_config = $shipment->getShippingMethod()->getPlugin()->getConfiguration();
    $customer_config = $shipping_method_config['api_information']['customer'];

    $customer_value = (new CustomerValue)
      ->setCustomerPreAlert()
      ->setCustomerCivility($customer_config['civility'])
      ->setCustomerName($customer_config['address']['given_name'])
      ->setCustomerName2($customer_config['address']['family_name'])
      ->setCustomerContactName($customer_config['address']['given_name'])
      ->setCustomerEmail($customer_config['email'])
      ->setCustomerAdress1($customer_config['address']['address_line1'])
      ->setCustomerAdress2($customer_config['address']['address_line2'])
      ->setCustomerCity($customer_config['address']['locality'])
      ->setCustomerZipCode($customer_config['address']['postal_code'])
      ->setCustomerCountry($customer_config['address']['country_code'])
      ->setCustomerCountryName($country_list[$customer_config['address']['country_code']]->render())
      ->setCustomerPhone($customer_config['mobile_phone'])
      // ->setCustomerMobilePhone($customer_config['mobile_phone']) // @todo Add it once supported.
      ->setPrintAsSender($customer_config['print_as_sender']);

    return $customer_value;
  }

}

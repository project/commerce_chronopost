<?php

declare(strict_types=1);

namespace Drupal\commerce_chronopost\Factory\ShippingServiceWS;

use Drupal\physical\LengthUnit;
use Drupal\physical\WeightUnit;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\Chronopost\ShippingServiceWS\StructType\SkybillWithDimensionsValueV6;

/**
 * SkybillWithDimensionsValueV6 factory.
 */
class SkybillWithDimensionsValueV6Factory extends SkybillWithDimensionsValueV6 {

  /**
   * Constructs a new SkybillWithDimensionsValueV6 instance from a shipment entity.
   *
   * @param ShipmentInterface $shipment
   *
   * @return SkybillWithDimensionsValueV6
   */
  public static function createFromShipment(ShipmentInterface $shipment): SkybillWithDimensionsValueV6 {
    $package = $shipment->getPackageType();
    $skybill_value = (new SkybillWithDimensionsValueV6)
      ->setObjectType('MAR')
      ->setService(self::getServiceFromShipment($shipment))
      ->setProductCode($shipment->getShippingService())
      ->setHeight((float) $package->getHeight()->convert(LengthUnit::CENTIMETER)->getNumber())
      ->setWidth((float) $package->getWidth()->convert(LengthUnit::CENTIMETER)->getNumber())
      ->setLength((float) $package->getLength()->convert(LengthUnit::CENTIMETER)->getNumber())
      ->setWeight((float) $shipment->getWeight()->convert(WeightUnit::KILOGRAM)->getNumber())
      ->setEvtCode('DC');

    return $skybill_value;
  }

  /**
   *
   */
  public static function getServiceFromShipment(ShipmentInterface $shipment): string {
    $weight = $shipment->getWeight()->convert(WeightUnit::KILOGRAM)->getNumber();

    switch ($shipment->getShippingService()) {
      case '2R':
      case '1T':
      case '5T':
        return $shipment->getShippingMethod()
          ->getPlugin()
          ->getConfiguration()['api_information']['skybill']['service'];

      break;

      case '4X':
        return '112';

      break;

      case '44':
        $shipping_address = $shipment
          ->getShippingProfile()
          ->get('address')
          ->first()
          ->getValue();
        if (!empty($shipping_address['organization'])) {
          return ($weight <= 3) ? '328' : '327';
        }
        else {
          return ($weight <= 3) ? '136' : '101';
        }
        break;

      default:
        return '';
      break;
    }
  }

}

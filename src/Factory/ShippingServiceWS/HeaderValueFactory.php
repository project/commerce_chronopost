<?php

declare(strict_types=1);

namespace Drupal\commerce_chronopost\Factory\ShippingServiceWS;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\Chronopost\ShippingServiceWS\StructType\HeaderValue;

/**
 * HeaderValue Factory.
 */
class HeaderValueFactory extends HeaderValue {

  /**
   *  Constructs a new HeaderValue instance from a shipment entity.
   *
   * @param ShipmentInterface $shipment
   *
   * @return HeaderValue
   */
  public static function createFromShipment(ShipmentInterface $shipment): HeaderValue {
    $shipping_method = $shipment->getShippingMethod();
    $shipping_method_config = $shipping_method->getPlugin()->getConfiguration();
    $mode = $shipping_method_config['mode'];
    $header_value = (new HeaderValue)
      ->setAccountNumber((int) $shipping_method_config['api_information']['credentials'][$mode]['account_number'])
      ->setIdEmit('CHRFR');

    return $header_value;
  }

}

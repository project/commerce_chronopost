<?php

declare(strict_types=1);

namespace Drupal\commerce_chronopost\Factory\ShippingServiceWS;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\Chronopost\ShippingServiceWS\StructType\RecipientValueV2;

/**
 * TRecipientValueV2 factory.
 */
class RecipientValueV2Factory extends RecipientValueV2 {

  /**
   * Constructs a new RecipientValueV2 instance from a shipment entity.
   *
   * @param ShipmentInterface $shipment
   *
   * @return RecipientValueV2
   */
  public static function createFromShipment(ShipmentInterface $shipment): RecipientValueV2 {
    $recipient_value = new RecipientValueV2();

    $order = $shipment->getOrder();
    $shipping_profile = $shipment->getShippingProfile();
    $shipping_address = $shipping_profile->get('address')->first()->getValue();
    $country_code = $shipping_address['country_code'];
    $country_list = \Drupal::getContainer()->get('country_manager')->getList();

    $recipient_name = $shipping_address['given_name'] . ' ' . $shipping_address['family_name'];
    $recipient_name2 = '';

    if ($organization = $shipping_address['organization']) {
      $recipient_name2 = $recipient_name;
      $recipient_name = $organization;
    }

    $recipient_value
      ->setRecipientType(self::getRecipientTypeFormShipment($shipment))
      ->setRecipientContactName($recipient_name2 ? $recipient_name2 : $recipient_name)
      ->setRecipientName($recipient_name)
      ->setRecipientName2($recipient_name2)
      ->setRecipientAdress1($shipping_address['address_line1'])
      ->setRecipientAdress2($shipping_address['address_line2'] ? $shipping_address['address_line2'] : '')
      ->setRecipientCity($shipping_address['locality'])
      ->setRecipientZipCode($shipping_address['postal_code'])
      ->setRecipientCountry($shipping_address['country_code'])
      ->setRecipientCountryName($country_list[$country_code]->render())
      ->setRecipientEmail($order->getEmail())
      ->setRecipientPhone('0000000000')
      // ->setRecipientMobilePhone($shipping_profile->get('field_mobile_phone')->value) // @todo Add it once supported.
      ->setRecipientPreAlert(22);

    return $recipient_value;
  }

  /**
   *
   */
  public static function getRecipientTypeFormShipment(ShipmentInterface $shipment): string {
    $shipping_profile = $shipment->getShippingProfile();
    $shipping_address = $shipping_profile->get('address')->first()->getValue();

    return empty($shipping_address['organization']) ? '2' : '1';
  }

}

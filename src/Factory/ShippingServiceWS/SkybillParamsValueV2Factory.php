<?php

declare(strict_types=1);

namespace Drupal\commerce_chronopost\Factory\ShippingServiceWS;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\Chronopost\ShippingServiceWS\StructType\SkybillParamsValueV2;

/**
 * SkybillParamsValueV2 factory.
 */
class SkybillParamsValueV2Factory extends SkybillParamsValueV2 {

  /**
   * Constructs a new SkybillParamsValueV2 instance from a shipment entity.
   *
   * @param ShipmentInterface $shipment
   *
   * @return SkybillParamsValueV2
   */
  public static function createFromShipment(ShipmentInterface $shipment): SkybillParamsValueV2 {
    $skybill_params_value = (new SkybillParamsValueV2)
      ->setMode('PDF');

    return $skybill_params_value;
  }

}

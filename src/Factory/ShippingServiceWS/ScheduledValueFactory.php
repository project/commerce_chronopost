<?php

declare(strict_types=1);

namespace Drupal\commerce_chronopost\Factory\ShippingServiceWS;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\Chronopost\ShippingServiceWS\StructType\ScheduledValue;

/**
 * ScheduledValue factory.
 */
class ScheduledValueFactory extends ScheduledValue {

  /**
   * Constructs a new ScheduledValue instance from a shipment entity.
   *
   * @param ShipmentInterface $shipment
   *
   * @return ScheduledValue
   */
  public static function createFromShipment(ShipmentInterface $shipment): ScheduledValue {
    $scheduled_value = (new ScheduledValue());

    return $scheduled_value;
  }

}

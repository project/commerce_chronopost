<?php

declare(strict_types=1);

namespace Drupal\commerce_chronopost\Factory\TrackingServiceWS;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\Chronopost\TrackingServiceWS\StructType\TrackSkybillV2;

/**
 * TrackSkybillV2 factory.
 */
class TrackSkybillV2Factory extends TrackSkybillV2 {

  /**
   * Constructs a new TrackSkybillV2 instance from a shipment entity.
   *
   * @param ShipmentInterface $commerce_shipment
   *
   * @return TrackSkybillV2
   */
  public static function createFromShipment(ShipmentInterface $commerce_shipment): TrackSkybillV2 {
    $track_skybill = (new TrackSkybillV2)
      ->setLanguage('fr_FR')
      ->setSkybillNumber($commerce_shipment->getTrackingCode());

    return $track_skybill;
  }

}

<?php

declare(strict_types=1);

namespace Drupal\commerce_chronopost\Factory\TrackingServiceWS;

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Maetva\Chronopost\TrackingServiceWS\StructType\CancelSkybill;

/**
 * CancelSkybill factory.
 */
class CancelSkybillFactory extends CancelSkybill {

  /**
   * Constructs a new CancelSkybill instance from a shipment entity.
   *
   * @param ShipmentInterface $shipment
   *
   * @return CancelSkybill
   */
  public static function createFromShipment(ShipmentInterface $shipment): CancelSkybill {
    $shipping_method_config = $shipment->getShippingMethod()->getPlugin()->getConfiguration();
    $mode = $shipping_method_config['mode'];

    $cancel_skybill = (new CancelSkybill)
      ->setAccountNumber($shipping_method_config['api_information']['credentials'][$mode]['account_number'])
      ->setPassword($shipping_method_config['api_information']['credentials'][$mode]['password'])
      ->setSkybillNumber($shipment->getTrackingCode());

    return $cancel_skybill;
  }

}

<?php

declare(strict_types = 1);

namespace Drupal\commerce_chronopost\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_chronopost\Factory\ShippingServiceWS\ShippingMultiParcelV4Factory;

/**
 * Manages the Chronopost shipping request form.
 */
class ShippingMultiParcelV4Form extends FormBase {

  /**
   * The module related logger channel ID.
   */
  const LOGGER_CHANNEL = 'commerce_chronopost';

  /**
   * The form ID.
   */
  const FORM_ID = 'commerce_chronopost_form';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The module related logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannel;

  /**
   * Create a new ShippingServiceWSController instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger channel factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger, LoggerChannelFactoryInterface $logger_channel_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->loggerChannel = $logger_channel_factory->get(self::LOGGER_CHANNEL);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('messenger'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}.
   */
  public function getFormId(): string {
    return self::FORM_ID;
  }

  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state, ShipmentInterface $commerce_shipment = NULL): array {
    $shipment = $commerce_shipment;
    $form['#shipment_entity'] = $shipment;

    $form['ship_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Ship date'),
      '#description' => $this->t('Leave empty to put the current date.'),
    ];

    if (in_array($shipment->getShippingService(), $shipment->getShippingMethod()->getPlugin()::FRESH_SERVICES)) {
      $form['expiration_date'] = [
        '#type' => 'date',
        '#required' => TRUE,
        '#title' => $this->t('Expiration date'),
      ];
      $form['sell_by_date'] = [
        '#type' => 'date',
        '#title' => $this->t('Sell by date'),
      ];
    }

    $form['actions'] = $this->actions($form, $form_state);

    return $form;
  }

  /**
   * Builds the form actions.
   *
   * @param array $form
   *   The form buildable array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The buildable array of the form actions part.
   */
  protected function actions(array $form, FormStateInterface $form_state): array {
    if ($form['#shipment_entity']->getTrackingCode()) {
      $title = $this->t('Cancel existing shipment and create a new one');
    }
    else {
      $title = $this->t('Create new shipment');
    }

    $actions = [
      '#type' => 'submit',
      '#value' => $title,
    ];

    return $actions;
  }

  /**
   * Manages the form submission / Chronopos shipping request.
   *
   * @param array $form
   *   The form buildable array.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $shipment = $form['#shipment_entity'];
    // Start a new Chronopost shipping creation.
    // Prepare the Chronopost shipping.
    $shipping_request_params = ShippingMultiParcelV4Factory::createFromShipment($shipment);
    $scheduled_value = $shipping_request_params->getScheduledValue();
    $skybill_value = $shipping_request_params->getSkybillValue();
    // Populate input values.
    if ($expiration_date = $form_state->getValue('expiration_date')) {
      foreach ($scheduled_value as $i => $scheduled_value_item) {
        $scheduled_value[$i] = $scheduled_value_item
          ->setExpirationDate($expiration_date);
        if ($sell_by_date = $form_state->getValue('sell_by_date')) {
          $scheduled_value[$i] = $scheduled_value_item
            ->setSellByDate($sell_by_date);
        }
      }
    }
    if ($ship_date = $form_state->getValue('ship_date')) {
      foreach ($skybill_value as $i => $skybill_value_item) {
        $skybill_value[$i] = $skybill_value_item
          ->setShipDate($ship_date);
      }
    }

    $shipping_request_params
      ->setScheduledValue($scheduled_value)
      ->setSkybillValue($skybill_value);

    // Request a new Chronopost shipping.
    $response = $shipment->getShippingMethod()->getPlugin()->createRemote($shipment, $shipping_request_params);

    if ($error_code = $response->getErrorCode()) {
      // An error orccured.
      $this->messenger->addError(
        $this->t('The Chronopost webservice returned the following error: (@code) @message', [
          '@code' => $error_code,
          '@message' => $this->t($response->getErrorMessage()),
        ])
      );
      return;
    }

    // Success!
    $this->messenger->addMessage($this->t('Chronopost shipping created'));
    $form_state->setRedirect(
      'entity.commerce_shipment.canonical', [
        'commerce_order' => $shipment->getOrder()->id(),
        'commerce_shipment' => $shipment->id(),
      ]);
  }

}

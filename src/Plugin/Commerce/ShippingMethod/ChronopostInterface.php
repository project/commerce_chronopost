<?php

declare(strict_types = 1);

namespace Drupal\commerce_chronopost\Plugin\Commerce\ShippingMethod;

use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_shipping\ShippingService;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\SupportsTrackingInterface;
use Maetva\Chronopost\ShippingServiceWS\StructType\ShippingMultiParcelV4;
use Maetva\Chronopost\TrackingServiceWS\StructType\CancelSkybillResponse;
use Maetva\Chronopost\TrackingServiceWS\StructType\TrackSkybillV2Response;

/**
 *
 */
interface Chronopost extends ContainerFactoryPluginInterface, ShippingMethodInterface, SupportsTrackingInterface {

  /**
   * Refreshes the rating options on rating mode selection.
   *
   * @param array $form
   *   The form buildable array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function ajaxRefresh(array &$form, FormStateInterface $form_state);

  /**
   * Whether a shipping service applies to a shipment or not.
   */
  public function serviceApplies(ShipmentInterface $shipment, ShippingService $service): bool;

  /**
   * {@inheritdoc}
   */
  public function createRemote(ShipmentInterface $shipment, ShippingMultiParcelV4 $shipping_multi_parcel = NULL): void;

  /**
   * Gets a shipment Label filename.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   * @param string $skybill_number
   *   The AWB number.
   *
   * @return string
   *   The filename.
   */
  public static function getLabelFilename(ShipmentInterface $shipment, string $skybill_number): string;

  /**
   * Tracks a Chronopost shipment.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\Chronopost\TrackingServiceWS\StructType\TrackSkybillV2Response
   *   The skybill tracking request response.
   */
  public function track(ShipmentInterface $shipment): TrackSkybillV2Response;

  /**
   * Cancels a Chronopost shipment.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   *
   * @return \Maetva\Chronopost\TrackingServiceWS\StructType\cancelSkybillResponse
   *   The skybill cancellation request result.
   */
  public function cancelRemote(ShipmentInterface $shipment): CancelSkybillResponse;

}

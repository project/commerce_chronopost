<?php

declare(strict_types = 1);

namespace Drupal\commerce_chronopost\Plugin\Commerce\ShippingMethod;

use Drupal\Core\Url;
use Drupal\physical\WeightUnit;
use Drupal\commerce_price\Price;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\FileRepositoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\commerce_shipping\ShippingService;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_chronopost\ShippingServiceWS;
use Drupal\commerce_chronopost\TrackingServiceWS;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\state_machine\WorkflowManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use CommerceGuys\Addressing\AddressFormat\AddressField;
use CommerceGuys\Addressing\AddressFormat\FieldOverride;
use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Drupal\commerce_chronopost\Event\PreShipmentRequestEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_chronopost\Event\CommerceChronopostEvents;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Maetva\Chronopost\TrackingServiceWS\StructType\ResultTrackSkybillV2;
use Maetva\Chronopost\ShippingServiceWS\StructType\ShippingMultiParcelV4;
use Maetva\Chronopost\TrackingServiceWS\StructType\CancelSkybillResponse;
use Maetva\Chronopost\ShippingServiceWS\StructType\ResultMultiParcelValue;
use Maetva\Chronopost\TrackingServiceWS\StructType\TrackSkybillV2Response;
use Drupal\commerce_chronopost\Factory\TrackingServiceWS\CancelSkybillFactory;
use Drupal\commerce_chronopost\Factory\TrackingServiceWS\TrackSkybillV2Factory;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodBase;
use Maetva\Chronopost\ShippingServiceWS\StructType\ResultMultiParcelExpeditionValue;
use Drupal\commerce_chronopost\Factory\ShippingServiceWS\ShippingMultiParcelV4Factory;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\SupportsTrackingInterface;

/**
 * Provides the FlatRate shipping method.
 *
 * @CommerceShippingMethod(
 *   id = "chronopost",
 *   label = @Translation("Chronopost"),
 *   services = {
 *     "44" = @Translation("Chrono Classic"),
 *     "1T" = @Translation("Chrono 13H Instance Relais"),
 *     "5T" = @Translation("Chrono Ambient 13 instance Relais"),
 *     "4X" = @Translation("Chrono Fresh Classic"),
 *     "2R" = @Translation("Chrono Fresh 13"),
 *   }
 * )
 */
class Chronopost extends ShippingMethodBase implements SupportsTrackingInterface {

  /**
   * The module logger channel.
   */
  const LOGGER_CHANNEL = 'commerce_chronopost';

  /**
   * Max. weight in kilogram.
   */
  const MAX_WEIGHT = 30;

  /**
   * The shipment labels storage directory.
   */
  const LABEL_DIR = 'private://chronopost';

  /**
   * The Chronopost API version.
   */
  const VERSION = '2.0';

  /**
   * Chronofresh service IDs.
   */
  const FRESH_SERVICES = ['2R', '4X'];

  /**
   * Domestic service IDS.
   */
  const SUPPORTED_COUNTRIES = [
    '1T' => [
      'France' => 'FR',
    ],
    '5T' => [
      'France' => 'FR',
    ],
    '2R' => [
      'France' => 'FR',
    ],
    '4X' => [
      'Belgium' => 'BE',
      'Spain' => 'ES',
      'France' => 'FR',
    ],
    '44' => [
      'Austria' => 'AT',
      'Belgium' => 'BE',
      'Bulgaria' => 'BG',
      'Croatia' => 'HR',
      'Cyprus' => 'CY',
      'Czech Republic' => 'CZ',
      'Denmark' => 'DK',
      'Estonia' => 'EE',
      'Finland' => 'FI',
      'France' => 'FR',
      'Germany' => 'DE',
      'Greece' => 'GR',
      'Hungary' => 'HU',
      'Ireland' => 'IE',
      'Italy' => 'IT',
      'Latvia' => 'LV',
      'Lithuania' => 'LT',
      'Luxembourg' => 'LU',
      'Malta' => 'MT',
      'Netherlands' => 'NL',
      'Poland' => 'PL',
      'Portugal' => 'PT',
      'Romania' => 'RO',
      'Slovakia' => 'SK',
      'Slovenia' => 'SI',
      'Spain' => 'ES',
      'Sweden' => 'SE',
    ],
  ];

  /**
   * The Chronopost web service.
   *
   * @var \Drupal\commerce_chronopost\ShippingServiceWS
   */
  protected $shippingServiceWS;

  /**
   * The Chronopost web service.
   *
   * @var \Drupal\commerce_chronopost\TrackingServiceWS
   */
  protected $trackingServiceWS;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The module related logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannel;

  /**
   * The Drupal file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The file repository.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepository;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructs a new ShippingMethodBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_shipping\PackageTypeManagerInterface $package_type_manager
   *   The package type manager.
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   * @param \Drupal\commerce_chronopost\ShippingServiceWS $shipping_service_ws
   *   The shipping webservice.
   * @param \Drupal\commerce_chronopost\TrackingServiceWS $shipping_service_ws
   *   The shipping webservice.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger_channel_factory
   *   The logging channel factory.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   * @param \Drupal\file\FileRepositoryInterface $file_repository
   *   The file repository.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PackageTypeManagerInterface $package_type_manager, WorkflowManagerInterface $workflow_manager, ShippingServiceWS $shipping_service_ws, TrackingServiceWS $tracking_service_ws, EntityTypeManagerInterface $entity_type_manager, FileSystemInterface $file_system, FileRepositoryInterface $file_repository, LoggerChannelFactoryInterface $logger_channel_factory, EventDispatcherInterface $event_dispatcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $package_type_manager, $workflow_manager);

    $this->shippingServiceWS = $shipping_service_ws;
    $this->trackingServiceWS = $tracking_service_ws;
    $this->entityTypeManager = $entity_type_manager;
    $this->fileSystem = $file_system;
    $this->fileRepository = $file_repository;
    $this->loggerChannel = $logger_channel_factory->get(self::LOGGER_CHANNEL);
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.commerce_package_type'),
      $container->get('plugin.manager.workflow'),
      $container->get('commerce_chronopost.shipping_service_ws'),
      $container->get('commerce_chronopost.tracking_service_ws'),
      $container->get('entity_type.manager'),
      $container->get('file_system'),
      $container->get('file.repository'),
      $container->get('logger.factory'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'mode' => 'test',
      'api_information' => [
        'credentials' => [
          'test' => [
            'account_number' => NULL,
            'password' => '',
          ],
          'live' => [
            'account_number' => NULL,
            'password' => '',
          ],
        ],
        'customer' => [
          'civility' => '',
          'address' => [],
          'email' => '',
          'mobile_phone' => '',
          'print_as_sender' => 'N',
        ],
        'shipper' => [
          'civility' => '',
          'address' => [],
          'email' => '',
          'phone' => '',
          'pre_alert' => '11',
        ],
        'skybill' => [
          'service' => '0',
        ],
      ],
      'options' => [
        'tracking_url' => 'https://www.chronopost.fr/tracking-no-cms/suivi-page?listeNumerosLT=[tracking_code]',
      ],
      'rating' => [
        'mode' => 'flat',
        'options' => [
          'flat' => [
            'rate_amount' => NULL,
          ],
        ],
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Select all services by default.
    if (empty($this->configuration['services'])) {
      $service_ids = array_keys($this->services);
      $this->configuration['services'] = array_combine($service_ids, $service_ids);
    }
    $is_new = $form_state->getBuildInfo()['callback_object']->getEntity()->isNew();

    $form['mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Mode'),
      '#description' => $this->t('Choose whether to use the test or live mode.'),
      '#options' => [
        'test' => $this->t('Test'),
        'live' => $this->t('Live'),
      ],
      '#default_value' => $this->configuration['mode'],
    ];

    $form['api_information'] = [
      '#type' => 'details',
      '#title' => $this->t('API information'),
      '#open' => $is_new,
    ];

    $form['api_information']['credentials'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Credentials'),
    ];

    foreach (['test', 'live'] as $mode) {
      $form['api_information']['credentials'][$mode] = [
        '#type' => 'fieldset',
        '#title' => $this->t(ucfirst($mode) . ' credentials'),
      ];

      $form['api_information']['credentials'][$mode]['account_number'] = [
        '#type' => 'number',
        '#title' => $this->t('Account number'),
        '#default_value' => $this->configuration['api_information']['credentials'][$mode]['account_number'],
        '#required' => TRUE,
      ];

      $form['api_information']['credentials'][$mode]['password'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Password'),
        '#default_value' => $this->configuration['api_information']['credentials'][$mode]['password'],
        '#required' => TRUE,
      ];
    }

    $form['api_information']['customer'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Customer'),
    ];

    $form['api_information']['customer']['civility'] = [
      '#type' => 'select',
      '#options' => [
        'L' => $this->t('Miss'),
        'E' => $this->t('Mrs'),
        'M' => $this->t('Sir'),
      ],
      '#title' => $this->t('Civility'),
      '#default_value' => $this->configuration['api_information']['customer']['civility'],
      '#required' => TRUE,
    ];

    $form['api_information']['customer']['address'] = [
      '#type' => 'address',
      '#title' => $this->t('Address'),
      '#default_value' => $this->configuration['api_information']['customer']['address'],
      '#required' => TRUE,
      '#field_overrides' => [
        AddressField::ADMINISTRATIVE_AREA => FieldOverride::HIDDEN,
        AddressField::DEPENDENT_LOCALITY => FieldOverride::HIDDEN,
        AddressField::SORTING_CODE => FieldOverride::HIDDEN,
        AddressField::ADDITIONAL_NAME => FieldOverride::HIDDEN,
        AddressField::ORGANIZATION => FieldOverride::HIDDEN,
      ],
    ];

    $form['api_information']['customer']['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#default_value' => $this->configuration['api_information']['customer']['email'],
      '#required' => TRUE,
    ];

    $form['api_information']['customer']['mobile_phone'] = [
      '#type' => 'tel',
      '#title' => $this->t('Mobile phone'),
      '#default_value' => $this->configuration['api_information']['customer']['mobile_phone'],
      '#required' => TRUE,
    ];

    $form['api_information']['customer']['print_as_sender'] = [
      '#type' => 'radios',
      '#options' => [
        'Y' => $this->t('Yes'),
        'N' => $this->t('No'),
      ],
      '#title' => $this->t('Print as sender'),
      '#default_value' => $this->configuration['api_information']['customer']['print_as_sender'],
      '#required' => TRUE,
    ];

    $form['api_information']['shipper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Shipper'),
    ];

    $form['api_information']['shipper']['civility'] = [
      '#type' => 'select',
      '#options' => [
        'L' => $this->t('Miss'),
        'E' => $this->t('Mrs'),
        'M' => $this->t('Sir'),
      ],
      '#title' => $this->t('Civility'),
      '#default_value' => $this->configuration['api_information']['shipper']['civility'],
      '#required' => TRUE,
    ];

    $form['api_information']['shipper']['address'] = [
      '#type' => 'address',
      '#title' => $this->t('Address'),
      '#default_value' => $this->configuration['api_information']['shipper']['address'],
      '#required' => TRUE,
      '#field_overrides' => [
        AddressField::ADMINISTRATIVE_AREA => FieldOverride::HIDDEN,
        AddressField::DEPENDENT_LOCALITY => FieldOverride::HIDDEN,
        AddressField::SORTING_CODE => FieldOverride::HIDDEN,
        AddressField::ADDITIONAL_NAME => FieldOverride::HIDDEN,
        AddressField::ORGANIZATION => FieldOverride::REQUIRED,
      ],
    ];

    $form['api_information']['shipper']['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#default_value' => $this->configuration['api_information']['shipper']['email'],
      '#required' => TRUE,
    ];

    $form['api_information']['shipper']['phone'] = [
      '#type' => 'tel',
      '#title' => $this->t('Phone'),
      '#default_value' => $this->configuration['api_information']['shipper']['phone'],
      '#required' => TRUE,
    ];

    $form['api_information']['shipper']['pre_alert'] = [
      '#type' => 'select',
      '#title' => $this->t('Prealert type'),
      '#options' => [
        '0' => $this->t('No Prealert'),
        '11' => $this->t('Shipper email prealert'),
      ],
      '#default_value' => $this->configuration['api_information']['shipper']['pre_alert'],
    ];

    $form['api_information']['skybill'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Skybill'),
    ];

    $form['api_information']['skybill']['service'] = [
      '#type' => 'radios',
      '#options' => [
        '0' => $this->t('Week delivery'),
        '1' => $this->t('Saturday delivery'),
      ],
      '#title' => $this->t('Service'),
      '#default_value' => $this->configuration['api_information']['skybill']['service'],
      '#required' => TRUE,
    ];

    $form['options'] = [
      '#type' => 'details',
      '#title' => $this->t('Options'),
      '#open' => $is_new,
    ];

    $form['options']['tracking_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tracking URL base'),
      '#description' => $this->t(
        'The base URL for assembling a tracking URL. If the [tracking_code]
        token is omitted, the code will be appended to the end of the URL
        (e.g. "https://www.chronopost.fr/tracking-no-cms/suivi-page?listeNumerosLT=123456789")'
      ),
      '#default_value' => $this->configuration['options']['tracking_url'],
    ];

    $form['rating'] = [
      '#type' => 'details',
      '#title' => $this->t('Rating'),
      '#open' => TRUE,
    ];
    $form['rating']['mode'] = [
      '#type' => 'radios',
      '#default_value' => $this->configuration['rating']['mode'],
      '#options' => [
        'flat' => $this->t('Flat rate'),
      ],
      '#title' => $this->t('Rating'),
      '#ajax' => [
        'wrapper' => 'rating_options',
        'callback' => [$this, 'ajaxRefresh'],
      ],
    ];
    $form['rating']['options'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'rating_options',
      ],
    ];

    $flat_type = $form_state->getValue(
      array_merge($form['#parents'], ['rating', 'mode'])
    );

    switch ($flat_type) {
      default:
      case 'flat':
        $amount = $this->configuration['rating']['options']['flat']['rate_amount'];
        // A bug in the plugin_select form element causes $amount to be incomplete.
        if (isset($amount) && !isset($amount['number'], $amount['currency_code'])) {
          $amount = NULL;
        }

        $form['rating']['options']['flat']['rate_amount'] = [
          '#type' => 'commerce_price',
          '#title' => $this->t('Rate amount'),
          '#default_value' => $amount,
          '#required' => TRUE,
        ];
        break;
    }

    return $form;
  }

  /**
   * Refreshes the rating options on rating mode selection.
   *
   * @param array $form
   *   The form buildable array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function ajaxRefresh(array &$form, FormStateInterface $form_state) {
    return $form['plugin']['widget'][0]['target_plugin_configuration']['form']['rating']['options'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['mode'] = $values['mode'];
      $this->configuration['api_information']['skybill']['service'] = $values['api_information']['skybill']['service'];
      foreach (['test', 'live'] as $mode) {
        $this->configuration['api_information']['credentials'][$mode]['account_number'] = $values['api_information']['credentials'][$mode]['account_number'];
        $this->configuration['api_information']['credentials'][$mode]['password'] = $values['api_information']['credentials'][$mode]['password'];
      }
      $this->configuration['api_information']['customer']['civility'] = $values['api_information']['customer']['civility'];
      $this->configuration['api_information']['customer']['address'] = $values['api_information']['customer']['address'];
      $this->configuration['api_information']['customer']['email'] = $values['api_information']['customer']['email'];
      $this->configuration['api_information']['customer']['mobile_phone'] = $values['api_information']['customer']['mobile_phone'];
      $this->configuration['api_information']['customer']['print_as_sender'] = $values['api_information']['customer']['print_as_sender'];
      $this->configuration['api_information']['shipper']['civility'] = $values['api_information']['shipper']['civility'];
      $this->configuration['api_information']['shipper']['address'] = $values['api_information']['shipper']['address'];
      $this->configuration['api_information']['shipper']['email'] = $values['api_information']['shipper']['email'];
      $this->configuration['api_information']['shipper']['phone'] = $values['api_information']['shipper']['phone'];
      $this->configuration['api_information']['shipper']['pre_alert'] = $values['api_information']['shipper']['pre_alert'];
      $this->configuration['options']['tracking_url'] = $values['options']['tracking_url'];
      $this->configuration['rating']['mode'] = $values['rating']['mode'];
      $this->configuration['rating']['options']['flat']['rate_amount'] = $values['rating']['options']['flat']['rate_amount'];
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getTrackingUrl(ShipmentInterface $shipment): ?Url {
    $code = $shipment->getTrackingCode();

    if (!empty($code)) {
      // If the tracking code token exists, replace it with the code.
      if (strstr($this->configuration['options']['tracking_url'], '[tracking_code]')) {
        $url = str_replace('[tracking_code]', $code, $this->configuration['options']['tracking_url']);
        return Url::fromUri($url);
      }

      // Otherwise, append the tracking code to the end of the URL.
      $url = $this->configuration['options']['tracking_url'] . $code;
      return Url::fromUri($url);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateRates(ShipmentInterface $shipment) {
    $rates = [];

    switch ($this->configuration['rating']['mode']) {
      case 'flat':
        foreach ($this->configuration['services'] as $service_id) {
          if ($this->serviceApplies($shipment, $this->services[$service_id])) {
            $rates[] = new ShippingRate([
              'shipping_method_id' => $this->parentEntity->id(),
              'service' => $this->services[$service_id],
              'amount' => Price::fromArray($this->configuration['rating']['options']['flat']['rate_amount']),
              'description' => $this->services[$service_id]->getLabel(),
            ]);
          }
        }
        break;

      default:
        break;
    }

    return $rates;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(ShipmentInterface $shipment) {
    if (!$weight = $shipment->getWeight()) {
      return FALSE;
    }

    return $weight->convert(WeightUnit::KILOGRAM)->getNumber() <= self::MAX_WEIGHT;
  }

  /**
   * {@inheritDoc}
   */
  public function serviceApplies(ShipmentInterface $shipment, ShippingService $service) {
    $shipping_address = $shipment->getShippingProfile()->get('address')->first()->getValue();

    return in_array(
      $shipping_address['country_code'],
      self::SUPPORTED_COUNTRIES[$service->getId()]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function createRemote(ShipmentInterface $shipment, ShippingMultiParcelV4 $shipping_multi_parcel = NULL): ?ResultMultiParcelExpeditionValue {
    if (!$shipping_multi_parcel) {
      $shipping_multi_parcel = ShippingMultiParcelV4Factory::createFromShipment($shipment);
    }
    $event = new PreShipmentRequestEvent($shipment, $shipping_multi_parcel);
    $this->eventDispatcher->dispatch($event, CommerceChronopostEvents::PRE_SHIPMENT_REQUEST);
    $shipping_response = $this->shippingServiceWS
      ->shippingMultiParcelV4($event->getParameters())
      ->getReturn();

    if ($remotes = $shipping_response->getResultMultiParcelValue()) {
      $this->processRemoteCreation($shipment, reset($remotes));
    }

    return $shipping_response;
  }

  /**
   * Processes a successful shipment request.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   * @param \Maetva\Chronopost\ShippingServiceWS\StructType\ResultMultiParcelValue $result
   *   The request result.
   */
  protected function processRemoteCreation(ShipmentInterface $shipment, ResultMultiParcelValue $result): void {
    $skybill_number = $result->getSkybillNumber();
    $this->setShipmentTrackingCode($shipment, $skybill_number);
    $this->storeShipmentLabel(
      $shipment,
      $skybill_number,
      $result->getPdfEtiquette()
    );
    $this->transitionateToPacked($shipment);
    $this->logShipment(
      $shipment->getOrder(),
      $shipment->id(),
      $skybill_number
    );
  }

  /**
   * Sets the shipment tracking code.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   * @param string $awb_number
   *   The AWB number.
   */
  protected function setShipmentTrackingCode(ShipmentInterface $shipment, string $tracking_code): void {
    $shipment->setTrackingCode($tracking_code);
    $shipment->save();
  }

  /**
   * Log the shipment request into the shipment related order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The shipment related order entity.
   * @param string $shipment_id
   *   The shipment shipment id.
   * @param string $awb_number
   *   The AWB number.
   * @param string $tracking_number
   *   The tracking number.
   */
  protected function logShipment(OrderInterface $order, string $shipment_id, string $tracking_number): void {
    $this->entityTypeManager
      ->getStorage('commerce_log')
      ->generate($order, 'commerce_chronopost_shipment_created', [
        'commerce_shipment_id' => $shipment_id,
        'skybill_number' => $tracking_number,
      ])
      ->save();
  }

  /**
   * Stores the shipment PDF label.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   * @param string $awb_number
   *   The tracking number.
   * @param string $label_image
   *   The label image object.
   */
  protected function storeShipmentLabel(ShipmentInterface $shipment, string $skybill_number, string $label_image): void {
    $pdf_label_dir = self::LABEL_DIR;
    $this->fileSystem->prepareDirectory(
      $pdf_label_dir,
      FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS
    );
    $this->fileRepository->writeData(
      $label_image,
      $pdf_label_dir . '/' . self::getLabelFilename($shipment, $skybill_number),
      FileSystemInterface::EXISTS_REPLACE
    );
  }

  /**
   * {@inheritDoc}
   */
  public static function getLabelFilename(ShipmentInterface $shipment, string $skybill_number): string {
    return implode('-', [
      $shipment->getOrder()->id(),
      $shipment->id(),
      $skybill_number,
    ]) . '.pdf';
  }

  /**
   * Set the shipment entity state.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   */
  protected function transitionateToPacked(ShipmentInterface $shipment): void {
    if ($shipment->getState()->isTransitionAllowed('pack')) {
      $shipment->getState()->applyTransitionById('pack');
      $shipment->save();
    }
  }

  /**
   * {@inheritDoc}
   */
  public function track(ShipmentInterface $shipment): TrackSkybillV2Response {
    try {
      $response = $this->trackingServiceWS->trackSkybillV2(TrackSkybillV2Factory::createFromShipment($shipment));
      $this->processTrackingResult($shipment, $response->getReturn());
    }
    catch (\Exception $e) {
      $this->loggerChannel->error($e->getMessage());
    }

    return $response;
  }

  /**
   *
   */
  protected function processTrackingResult(ShipmentInterface $shipment, ResultTrackSkybillV2 $result) {
    if ($events = $result->getListEventInfoComp()->getEvents()) {
      $event = end($events);
      $this->transitionateFromTrackingEvent($shipment, trim($event->getCode()));
    }
  }

  /**
   * Set the shipment entity state according to the last event code.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment entity.
   * @param string $event_code
   *   The DHL event code.
   */
  protected function transitionateFromTrackingEvent(ShipmentInterface $shipment, string $event_code): void {
    switch ($event_code) {
      case 'D':
      case 'D1':
      case 'D2':
      case 'D6':
      case 'D7':
      case 'DD':
        if ($shipment->getState()->isTransitionAllowed('deliver')) {
          $shipment->getState()->applyTransitionById('deliver');
          $shipment->save();
        }
        break;

      default:
        if ($shipment->getState()->isTransitionAllowed('ship')) {
          $shipment->getState()->applyTransitionById('ship');
          $shipment->save();
        }
        break;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function cancelRemote(ShipmentInterface $shipment): CancelSkybillResponse {
    try {
      $response = $this->trackingServiceWS
        ->cancelSkybill(CancelSkybillFactory::createFromShipment($shipment));
    }
    catch (\Exception $e) {
      $this->loggerChannel->error($e->getMessage());
    }

    return $response;
  }

}

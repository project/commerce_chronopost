<?php

declare(strict_types = 1);

namespace Drupal\commerce_chronopost\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\commerce_chronopost\TrackingServiceWS;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a DHL shipments queue worker.
 *
 * @QueueWorker(
 *  id = "commerce_chronopost_tracking",
 *  title = @Translation("Chronopost shipments tracking"),
 *  cron = {"time" = 30}
 * )
 */
class TrackingWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The TrackingServiceWS service.
   *
   * @var \Drupal\commerce_chronopost\TrackingServiceWS
   */
  protected $trackingServiceWS;

  /**
   * Constructs a new Tracking worker object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_chronopost\TrackingServiceWS $tracking_service_ws
   *   The DHL tracking service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TrackingServiceWS $tracking_service_ws) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->trackingServiceWS = $tracking_service_ws;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('commerce_chronopost.tracking_service_ws')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($shipments): void {
    foreach ($shipments as $shipment) {
      $shipment->getShippingMethod()->getPlugin()->track($shipment);
    }
  }

}

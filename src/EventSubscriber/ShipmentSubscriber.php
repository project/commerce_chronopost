<?php

declare(strict_types = 1);

namespace Drupal\commerce_chronopost\EventSubscriber;

use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 *
 */
class ShipmentSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'commerce_shipment.deliver.post_transition' => ['onDeliver'],
    ];
  }

  /**
   * Complete the shipment's order when the shipment is delivered.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   */
  public function onDeliver(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity()->getOrder();

    if (!$order->getState()->isTransitionAllowed('fulfill')) {
      return;
    }
    $order->getState()->applyTransitionById('fulfill');
    $order->save();
  }

}

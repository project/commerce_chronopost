<?php

/**
 * @file
 * Contains commerce_chronopost.module.
 */

declare(strict_types=1);

use Drupal\Core\Url;
use Drupal\Core\Entity\EntityInterface;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;

/**
 * Implements hook_entity_operation().
 */
function commerce_chronopost_entity_operation(EntityInterface $entity): array {
  $operations = [];

  if (!$entity instanceof ShipmentInterface) {
    return $operations;
  }

  $commerce_shipment = $entity;
  $shipping_method = $entity->getShippingMethod();

  if (!$shipping_method || $shipping_method->getPlugin()->getPluginId() !== 'chronopost') {
    return $operations;
  }

  $state_id = $commerce_shipment->get('state')->value;

  if ($state_id === 'ready') {
      $operations['commerce_chronopost_shipping_create'] = [
      'title' => \t('Create Chronopost shipment'),
      'url' => Url::fromRoute(
          'commerce_chronopost.shipping.create_shipment',
          ['commerce_shipment' => $commerce_shipment->id()]
      ),
      'weight' => 10,
    ];
  }
  elseif ($state_id === 'packed') {
    $operations['commerce_chronopost_shipping_cancel'] = [
      'title' => \t('Cancel Chronopost shipment'),
      'url' => Url::fromRoute(
        'commerce_chronopost.tracking.cancel_shipment',
        ['commerce_shipment' => $commerce_shipment->id()]
      ),
      'weight' => 10,
    ];
  }

  if (in_array($state_id, ['packed', 'shipped', 'delivered'])) {
    $operations['commerce_chronopost_shipping_label'] = [
      'title' => \t('Download Chronopost label'),
      'url' => Url::fromRoute(
        'commerce_chronopost.shipping.download_label',
        ['commerce_shipment' => $commerce_shipment->id()]
      ),
      'weight' => 10,
    ];
  }

  if (in_array($state_id, ['packed', 'shipped'])) {
    $operations['commerce_chronopost_shipping_track'] = [
      'title' => \t('Track Chronopost shipment'),
      'url' => Url::fromRoute(
        'commerce_chronopost.tracking.track_shipment',
        ['commerce_shipment' => $commerce_shipment->id()]
      ),
      'weight' => 10,
    ];
  }

  return $operations;
}

/**
 * Implements hook_cron().
 */
function commerce_chronopost_cron(): void {
  $shipment_ids = \Drupal::entityQuery('commerce_shipment')
    ->accessCheck()
    ->condition('tracking_code', NULL, 'IS NOT NULL')
    ->condition('state', ['packed', 'shipped'], 'IN')
    ->condition('shipping_method.entity:commerce_shipping_method.plugin.target_plugin_id', 'chronopost')
    ->execute();
  ;
  $shipments = \Drupal::entityTypeManager()
    ->getStorage('commerce_shipment')
    ->loadMultiple($shipment_ids);
  \Drupal::service('queue')
    ->get('commerce_chronopost_tracking')
    ->createItem($shipments);
}

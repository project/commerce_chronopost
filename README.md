# Drupal Commerce Chronopost

Provides Chronopost integration with Commerce Shipping.

## Setup

1. Install the module.
2. Create a shipping method using Chronopost.
3. Once an order is placed, visit the "Shipments" order tab to use Chronopost dedicated operations.

<?php

declare(strict_types=1);

namespace Drupal\commerce_chronopost\Factory\TrackingServiceWS;

use Drupal\Tests\commerce_chronopost\Kernel\ChronopostKernelTestBase;
use Drupal\commerce_chronopost\Factory\TrackingServiceWS\TrackSkybillV2Factory;
use Maetva\Chronopost\TrackingServiceWS\StructType\TrackSkybillV2;

/**
 * Tests the TrackSkybillV2 Factory.
 *
 * @coversDefaultClass \Drupal\commerce_chronopost\Factory\ShippingServiceWS\TrackSkybillV2Factory
 * @group commerce_chronopost
 */
class TrackSkybillV2FactoryTest extends ChronopostKernelTestBase
{
  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $track_skybill = TrackSkybillV2Factory::createFromShipment($this->shipment);

    $this->assertInstanceOf(TrackSkybillV2::class, $track_skybill);
    $this->assertEquals('fr_FR', $track_skybill->getLanguage());
    $this->assertEquals($this->shipment->getTrackingCode(), $track_skybill->getSkybillNumber());
  }
}

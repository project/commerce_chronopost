<?php

declare(strict_types=1);

namespace Drupal\commerce_chronopost\Factory\TrackingServiceWS;

use Drupal\Tests\commerce_chronopost\Kernel\ChronopostKernelTestBase;
use Drupal\commerce_chronopost\Factory\TrackingServiceWS\CancelSkybillFactory;
use Maetva\Chronopost\TrackingServiceWS\StructType\CancelSkybill;

/**
 * Tests the CancelSkybill Factory.
 *
 * @coversDefaultClass \Drupal\commerce_chronopost\Factory\ShippingServiceWS\CancelSkybillFactory
 * @group commerce_chronopost
 */
class CancelSkybillFactoryTest extends ChronopostKernelTestBase
{
  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $cancel_skybill = CancelSkybillFactory::createFromShipment($this->shipment);

    $this->assertInstanceOf(CancelSkybill::class, $cancel_skybill);

    $shipping_method_config = $this->shipment->getShippingMethod()->getPlugin()->getConfiguration();
    $mode = $shipping_method_config['mode'];

    $this->assertEquals(
      $shipping_method_config['api_information']['credentials'][$mode]['account_number'],
      $cancel_skybill->getAccountNumber()
    );
    $this->assertEquals(
      $shipping_method_config['api_information']['credentials'][$mode]['password'],
      $cancel_skybill->getPassword()
    );
    $this->assertEquals($this->shipment->getTrackingCode(), $cancel_skybill->getSkybillNumber());
  }
}

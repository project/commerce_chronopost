<?php

declare(strict_types=1);

namespace Drupal\commerce_chronopost\Factory\ShippingServiceWS;

use Drupal\Tests\commerce_chronopost\Kernel\ChronopostKernelTestBase;
use Maetva\Chronopost\ShippingServiceWS\StructType\RefValueV2;

/**
 * Tests the RefValueV2Factory Factory.
 *
 * @coversDefaultClass \Drupal\commerce_chronopost\Factory\ShippingServiceWS\RefValueV2Factory
 * @group commerce_chronopost
 */
class RefValueV2FactoryTest extends ChronopostKernelTestBase
{
  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $ref_value = RefValueV2Factory::createFromShipment($this->shipment);

    $order = $this->shipment->getOrder();

    $this->assertInstanceOf(RefValueV2::class, $ref_value);
    $this->assertEquals(
      $order->id() . '-' . $this->shipment->id(),
      $ref_value->getCustomerSkybillNumber());
    $this->assertEquals(
      $this->shipment->getOrder()->getStore()->id(),
      $ref_value->getShipperRef());
    $this->assertEquals(
      $order->getCustomer()->id() . '-' . $this->shipment->getShippingProfile()->id(),
      $ref_value->getRecipientRef()
    );
  }

}

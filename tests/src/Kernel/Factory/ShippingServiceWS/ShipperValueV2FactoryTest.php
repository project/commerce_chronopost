<?php

declare(strict_types=1);

namespace Drupal\commerce_chronopost\Factory\ShippingServiceWS;

use Drupal\Tests\commerce_chronopost\Kernel\ChronopostKernelTestBase;
use Maetva\Chronopost\ShippingServiceWS\StructType\ShipperValueV2;

/**
 * Tests the ShipperValueV2Factory Factory.
 *
 * @coversDefaultClass \Drupal\commerce_chronopost\Factory\ShippingServiceWS\ShipperValueV2Factory
 * @group commerce_chronopost
 */
class ShipperValueV2FactoryTest extends ChronopostKernelTestBase
{

  /**
   * The country manager.
   *
   * @var \Drupal\Core\Locale\CountryManagerInterface
   */
  protected $countryManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->countryManager = $this->container->get('country_manager');
  }

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $shipper_value = ShipperValueV2Factory::createFromShipment($this->shipment);

    $this->assertInstanceOf(ShipperValueV2::class, $shipper_value);

    $shipping_method_config = $this->shipment->getShippingMethod()->getPlugin()->getConfiguration();
    $shipper_config = $shipping_method_config['api_information']['shipper'];

    $this->assertEquals($shipper_config['civility'], $shipper_value->getShipperCivility());
    $this->assertEquals($shipper_config['address']['address_line1'], $shipper_value->getShipperAdress1());
    $this->assertEquals($shipper_config['address']['address_line2'], $shipper_value->getShipperAdress2());
    $this->assertEquals($shipper_config['address']['locality'], $shipper_value->getShipperCity());
    $this->assertEquals($shipper_config['address']['postal_code'], $shipper_value->getShipperZipCode());
    $this->assertEquals($shipper_config['address']['country_code'], $shipper_value->getShipperCountry());
    $this->assertEquals(
      $this->countryManager->getList()[$shipper_config['address']['country_code']]->render(),
      $shipper_value->getShipperCountryName()
    );
    $this->assertEquals($shipper_config['email'], $shipper_value->getShipperEmail());
    $this->assertEquals($shipper_config['phone'], $shipper_value->getShipperPhone());
    $this->assertEquals($shipper_config['address']['organization'], $shipper_value->getShipperName());
    $this->assertEquals(
      $shipper_config['address']['given_name'] . ' ' . $shipper_config['address']['family_name'],
      $shipper_value->getShipperContactName()
    );
    $this->assertEquals((int) $shipper_config['pre_alert'], $shipper_value->getShipperPreAlert());
  }
}

<?php

declare(strict_types=1);

namespace Drupal\commerce_chronopost\Factory\ShippingServiceWS;

use Drupal\Tests\commerce_chronopost\Kernel\ChronopostKernelTestBase;
use Maetva\Chronopost\ShippingServiceWS\StructType\ScheduledValue;

/**
 * Tests the ScheduledValueFactory Factory.
 *
 * @coversDefaultClass \Drupal\commerce_chronopost\Factory\ShippingServiceWS\ScheduledValueFactory
 * @group commerce_chronopost
 */
class ScheduledValueFactoryTest extends ChronopostKernelTestBase
{
  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $scheduled_value = ScheduledValueFactory::createFromShipment($this->shipment);

    $this->assertInstanceOf(ScheduledValue::class, $scheduled_value);
  }
}

<?php

declare(strict_types=1);

namespace Drupal\commerce_chronopost\Factory\ShippingServiceWS;

use Drupal\physical\LengthUnit;
use Drupal\Tests\commerce_chronopost\Kernel\ChronopostKernelTestBase;
use Maetva\Chronopost\ShippingServiceWS\StructType\EsdValue3;

/**
 * Tests the EsdValue3 Factory.
 *
 * @coversDefaultClass \Drupal\commerce_chronopost\Factory\ShippingServiceWS\EsdValue3Factory
 * @group commerce_chronopost
 */
class EsdValue3FactoryTest extends ChronopostKernelTestBase
{
  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $esd_value3 = EsdValue3Factory::createFromShipment($this->shipment);
    $this->assertInstanceOf(EsdValue3::class, $esd_value3);

    $package = $this->shipment->getPackageType();
    $this->assertEquals(
      (float) $package->getHeight()->convert(LengthUnit::CENTIMETER)->getNumber(),
      $esd_value3->getHeight()
    );
    $this->assertEquals(
      (float) $package->getWidth()->convert(LengthUnit::CENTIMETER)->getNumber(),
      $esd_value3->getWidth()
    );
    $this->assertEquals(
      $package->getLength()->convert(LengthUnit::CENTIMETER)->getNumber(),
      $esd_value3->getLength()
    );
    $this->assertEquals(1, $esd_value3->getNombreDePassageMaximum());
    $this->assertEquals(FALSE, $esd_value3->getLtAImprimerParChronopost());
  }
}

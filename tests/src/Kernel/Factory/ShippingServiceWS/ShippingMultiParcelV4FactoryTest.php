<?php

declare(strict_types=1);

namespace Drupal\commerce_chronopost\Factory\ShippingServiceWS;

use Drupal\Tests\commerce_chronopost\Kernel\ChronopostKernelTestBase;
use Maetva\Chronopost\ShippingServiceWS\StructType\HeaderValue;
use Maetva\Chronopost\ShippingServiceWS\StructType\CustomerValue;
use Maetva\Chronopost\ShippingServiceWS\StructType\SkybillParamsValue;
use Maetva\Chronopost\ShippingServiceWS\StructType\ShippingMultiParcelV4;

/**
 * Tests the ShippingMultiParcelV4 Factory.
 *
 * @coversDefaultClass \Drupal\commerce_chronopost\Factory\ShippingServiceWS\ShippingMultiParcelV4Factory
 * @group commerce_chronopost
 */
class ShippingMultiParcelV4FactoryTest extends ChronopostKernelTestBase
{

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $shipping_multi_parcel = ShippingMultiParcelV4Factory::createFromShipment($this->shipment);

    $shipping_method_config = $this->shipment->getShippingMethod()->getPlugin()->getConfiguration();
    $mode = $shipping_method_config['mode'];

    $this->assertInstanceOf(ShippingMultiParcelV4::class, $shipping_multi_parcel);
    $this->assertInstanceOf(HeaderValue::class, $shipping_multi_parcel->getHeaderValue());
    $this->assertIsArray($shipping_multi_parcel->getShipperValue());
    $this->assertInstanceOf(CustomerValue::class, $shipping_multi_parcel->getCustomerValue());
    $this->assertIsArray($shipping_multi_parcel->getRecipientValue());
    $this->assertIsArray($shipping_multi_parcel->getRefValue());
    $this->assertIsArray($shipping_multi_parcel->getSkybillValue());
    $this->assertInstanceOf(SkybillParamsValue::class, $shipping_multi_parcel->getSkybillParamsValue());
    $this->assertEquals(
      $shipping_method_config['api_information']['credentials'][$mode]['password'],
      $shipping_multi_parcel->getPassword()
    );
    $this->assertIsArray($shipping_multi_parcel->getScheduledValue());
    $this->assertEquals(1, $shipping_multi_parcel->getNumberOfParcel());
    $this->assertEquals('1', $shipping_multi_parcel->getModeRetour());
  }
}

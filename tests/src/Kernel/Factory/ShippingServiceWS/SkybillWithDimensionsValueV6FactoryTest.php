<?php

declare(strict_types=1);

namespace Drupal\commerce_chronopost\Factory\ShippingServiceWS;

use Drupal\physical\LengthUnit;
use Drupal\physical\WeightUnit;
use Drupal\Tests\commerce_chronopost\Kernel\ChronopostKernelTestBase;
use Maetva\Chronopost\ShippingServiceWS\StructType\SkybillWithDimensionsValueV6;

/**
 * Tests the SkybillWithDimensionsValueV6 Factory.
 *
 * @coversDefaultClass \Drupal\commerce_chronopost\Factory\ShippingServiceWS\SkybillWithDimensionsValueV6Factory
 * @group commerce_chronopost
 */
class SkybillWithDimensionsValueV6FactoryTest extends ChronopostKernelTestBase
{

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $skybill_with_dimensions_value = SkybillWithDimensionsValueV6Factory::createFromShipment($this->shipment);

    $this->assertInstanceOf(SkybillWithDimensionsValueV6::class, $skybill_with_dimensions_value);

    $package = $this->shipment->getPackageType();

    $this->assertEquals('MAR', $skybill_with_dimensions_value->getObjectType());
    $this->assertEquals(
      SkybillWithDimensionsValueV6Factory::getServiceFromShipment($this->shipment),
      $skybill_with_dimensions_value->getService()
    );
    $this->assertEquals('MAR', $skybill_with_dimensions_value->getObjectType());
    $this->assertEquals(
      $this->shipment->getShippingService(),
      $skybill_with_dimensions_value->getProductCode()
    );
    $this->assertEquals(
      (float) $package->getHeight()->convert(LengthUnit::CENTIMETER)->getNumber(),
      $skybill_with_dimensions_value->getHeight()
    );
    $this->assertEquals(
      (float) $package->getWidth()->convert(LengthUnit::CENTIMETER)->getNumber(),
      $skybill_with_dimensions_value->getWidth()
    );
    $this->assertEquals(
      (float) $package->getLength()->convert(LengthUnit::CENTIMETER)->getNumber(),
      $skybill_with_dimensions_value->getLength()
    );
    $this->assertEquals(
      (float) $this->shipment->getWeight()->convert(WeightUnit::KILOGRAM)->getNumber(),
      $skybill_with_dimensions_value->getWeight()
    );
    $this->assertEquals('DC', $skybill_with_dimensions_value->getEvtCode());
  }

}

<?php

declare(strict_types=1);

namespace Drupal\commerce_chronopost\Factory\ShippingServiceWS;

use Drupal\Tests\commerce_chronopost\Kernel\ChronopostKernelTestBase;
use Drupal\commerce_chronopost\Factory\ShippingServiceWS\CustomerValueFactory;
use Maetva\Chronopost\ShippingServiceWS\StructType\CustomerValue;

/**
 * Tests the CustomerValue Factory.
 *
 * @coversDefaultClass \Drupal\commerce_chronopost\Factory\ShippingServiceWS\CustomerValueFactory
 * @group commerce_chronopost
 */
class CustomerValueFactoryTest extends ChronopostKernelTestBase
{
  /**
   * The country manager
   *
   * @var \Drupal\Core\Locale\CountryManagerInterface
   */
  protected $countryManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->countryManager = $this->container->get('country_manager');
  }

  /**
   * @covers ::createRecipientAddressFromShipment
   */
  public function testCreateFromShipment(): void {
    $customer_value = CustomerValueFactory::createFromShipment($this->shipment);

    $this->assertInstanceOf(CustomerValue::class, $customer_value);
    $shipping_method_config = $this->shipment->getShippingMethod()->getPlugin()->getConfiguration();
    $customer_config = $shipping_method_config['api_information']['customer'];

    $this->assertEquals($customer_config['civility'], $customer_value->getCustomerCivility());
    $this->assertEquals($customer_config['address']['given_name'], $customer_value->getCustomerName());
    $this->assertEquals($customer_config['address']['family_name'], $customer_value->getCustomerName2());
    $this->assertEquals($customer_config['address']['given_name'], $customer_value->getCustomerContactName());
    $this->assertEquals($customer_config['email'], $customer_value->getCustomerEmail());
    $this->assertEquals($customer_config['address']['address_line1'], $customer_value->getCustomerAdress1());
    $this->assertEquals($customer_config['address']['address_line2'], $customer_value->getCustomerAdress2());
    $this->assertEquals($customer_config['address']['locality'], $customer_value->getCustomerCity());
    $this->assertEquals($customer_config['address']['postal_code'], $customer_value->getCustomerZipCode());
    $this->assertEquals($customer_config['address']['country_code'], $customer_value->getCustomerCountry());
    $this->assertEquals(
      $this->countryManager->getList()[$customer_config['address']['country_code']],
      $customer_value->getCustomerCountryName()
    );
    $this->assertEquals($customer_config['mobile_phone'], $customer_value->getCustomerPhone());
    $this->assertEquals($customer_config['print_as_sender'], $customer_value->getPrintAsSender());
  }

}

<?php

declare(strict_types=1);

namespace Drupal\commerce_chronopost\Factory\ShippingServiceWS;

use Drupal\Tests\commerce_chronopost\Kernel\ChronopostKernelTestBase;

/**
 * Tests the HeaderValueFactory Factory.
 *
 * @coversDefaultClass \Drupal\commerce_chronopost\Factory\ShippingServiceWS\HeaderValueFactory
 * @group commerce_chronopost
 */
class HeaderValueFactoryTest extends ChronopostKernelTestBase
{
  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $header_value = HeaderValueFactory::createFromShipment($this->shipment);
    $shipping_method_config = $this->shipment->getShippingMethod()->getPlugin()->getConfiguration();
    $mode = $shipping_method_config['mode'];

    $this->assertEquals(
      (int) $shipping_method_config['api_information']['credentials'][$mode]['account_number'],
      $header_value->getAccountNumber()
    );
    $this->assertEquals('CHRFR', $header_value->getIdEmit());
  }
}

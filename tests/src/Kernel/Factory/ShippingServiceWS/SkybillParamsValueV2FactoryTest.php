<?php

declare(strict_types=1);

namespace Drupal\commerce_chronopost\Factory\ShippingServiceWS;

use Drupal\Tests\commerce_chronopost\Kernel\ChronopostKernelTestBase;
use Maetva\Chronopost\ShippingServiceWS\StructType\SkybillParamsValueV2;

/**
 * Tests the SkybillParamsValueV2 Factory.
 *
 * @coversDefaultClass \Drupal\commerce_chronopost\Factory\ShippingServiceWS\SkybillParamsValueV2Factory
 * @group commerce_chronopost
 */
class SkybillParamsValueV2FactoryTest extends ChronopostKernelTestBase
{
  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $skybill_params_value = SkybillParamsValueV2Factory::createFromShipment($this->shipment);

    $this->assertInstanceOf(SkybillParamsValueV2::class, $skybill_params_value);
    $this->assertEquals('PDF', $skybill_params_value->getMode());
  }
}

<?php

declare(strict_types=1);

namespace Drupal\commerce_chronopost\Factory\ShippingServiceWS;

use Drupal\Tests\commerce_chronopost\Kernel\ChronopostKernelTestBase;
use Drupal\commerce_chronopost\Factory\ShippingServiceWS\RecipientValueV2Factory;
use Maetva\Chronopost\ShippingServiceWS\StructType\RecipientValueV2;

/**
 * Tests the RecipientValueV2Factory Factory.
 *
 * @coversDefaultClass \Drupal\commerce_chronopost\Factory\ShippingServiceWS\RecipientValueV2Factory
 * @group commerce_chronopost
 */
class RecipientValueV2FactoryTest extends ChronopostKernelTestBase
{

  /**
   * The country manager.
   *
   * @var \Drupal\Core\Locale\CountryManagerInterface
   */
  protected $countryManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->countryManager = $this->container->get('country_manager');
  }

  /**
   * @covers ::createFromShipment
   */
  public function testCreateFromShipment(): void {
    $recipient_value = RecipientValueV2Factory::createFromShipment($this->shipment);

    $shipping_address = $this->shipment->getShippingProfile()->get('address')->first()->getValue();
    $country_list = $this->countryManager->getList();

    $recipient_name = $shipping_address['given_name'] . ' ' . $shipping_address['family_name'];
    $recipient_name2 = '';

    if ($organization = $shipping_address['organization']) {
      $recipient_name2 = $recipient_name;
      $recipient_name = $organization;
    }

    $this->assertInstanceOf(RecipientValueV2::class, $recipient_value);
    $this->assertEquals(
      RecipientValueV2Factory::getRecipientTypeFormShipment($this->shipment),
      $recipient_value->getRecipientType()
    );
    $this->assertEquals(
      $recipient_name2 ? $recipient_name2 : $recipient_name,
      $recipient_value->getRecipientContactName()
    );
    $this->assertEquals($recipient_name2, $recipient_value->getRecipientName2());
    $this->assertEquals($shipping_address['address_line1'], $recipient_value->getRecipientAdress1());
    $this->assertEquals(
      $shipping_address['address_line2'] ? $shipping_address['address_line2'] : '',
      $recipient_value->getRecipientAdress2()
    );
    $this->assertEquals($shipping_address['locality'], $recipient_value->getRecipientCity());
    $this->assertEquals($shipping_address['postal_code'], $recipient_value->getRecipientZipCode());
    $this->assertEquals($shipping_address['country_code'], $recipient_value->getRecipientCountry());
    $this->assertEquals($country_list[$shipping_address['country_code']]->render(), $recipient_value->getRecipientCountryName());
    $this->assertEquals($this->shipment->getOrder()->getEmail(), $recipient_value->getRecipientEmail());
    $this->assertEquals('0000000000', $recipient_value->getRecipientPhone());
    $this->assertEquals(22, $recipient_value->getRecipientPreAlert());
  }

}
